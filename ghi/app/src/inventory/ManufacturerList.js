import React, {useState, useEffect} from 'react';


function ManufactureList(props) {

return (
    <table className="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
            </tr>
        </thead>
		<tbody>
			{props.manufacturers.map(manufact => {
			return (
				<tr key={manufact.name}>
					<td>{ manufact.name }</td>
				</tr>
			);
			}

			)}
		</tbody>
    </table>
 );
}
export default ManufactureList;
