import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from "react";
import MainPage from './MainPage';
import Nav from './Nav';
import AddSalesPersonForm from './sales/AddSalesPersonForm';
import AddPotentialCustomerForm from './sales/AddNewCustomerForm';
import SalesPersonSales from './sales/SalesPersonSales';
import CreateSaleRecord from './sales/CreateSalesRecord';
import ListSales from './sales/ListSales';
import AppointmentForm from './service/AppointmentForm';
import AppointmentList from './service/AppointmentList';
import TechnicianForm from './service/TechnicianForm';
import ServiceHistory from './service/ServiceHistory';
import AutomobileForm from './inventory/AutomobileForm';
import AutomobileList from './inventory/AutomobileList';
import VehicleList from './inventory/VehicleList';
import VehicleForm from './inventory/VehicleForm';
import ManufactureForm from './inventory/ManufactureForm';
import ManufacturerList from './inventory/ManufacturerList';


function App() {
  const [manufacturers, setManufacturers] = useState([]);
  const fetchManufacturers = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
    }
  }


  useEffect(() => {
    fetchManufacturers();
  }, []);




  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="NewSalesPerson" element={<AddSalesPersonForm />} />
          <Route path="NewCustomer" element={<AddPotentialCustomerForm/>} />
          <Route path="SalesPersonSales" element={<SalesPersonSales/>} />
          <Route path="CreateSaleRecord" element={<CreateSaleRecord/>} />
          <Route path="AllSales" element={<ListSales/>} />
          <Route path="appointments/new" element={<AppointmentForm />} />
          <Route path="appointments/" element={<AppointmentList />} />
          <Route path="technicians/new/" element={<TechnicianForm />} />
          <Route path="history/" element={<ServiceHistory />} />
          <Route path="manufacturers" >
          <Route path="new" element={<ManufactureForm fetchManufacturers={fetchManufacturers} />} />
          <Route path="" element={<ManufacturerList manufacturers={manufacturers} />} />
          </Route>
          <Route path="createautomobile/" element={<AutomobileForm/>} />
          <Route path="automobilelist/" element={<AutomobileList/>} />
          <Route path="createvehicle/" element={<VehicleForm/>} />
          <Route path="vehiclelist/" element={<VehicleList/>} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
