import { NavLink, Link } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink
                className="nav-link"
                activeClassName="active"
                exact
                to="/"
              >
                Home
              </NavLink>
            </li>
            <li className="nav-item dropdown">
             <a
               className="nav-link dropdown-toggle"
               href="#"
               role="button"
               data-bs-toggle="dropdown"
               aria-expanded="false"
             >
               Inventory
             </a>
             <ul className="dropdown-menu">
               <li className="nav-item">
                 <NavLink
                   className="dropdown-item"
                   aria-current="page"
                   to="automobilelist/"
                 >
                   All Automobiles
                 </NavLink>
               </li>
               <li>
                 <NavLink
                   className="dropdown-item"
                   aria-current="page"
                   to="createautomobile/"
                 >
                   Add Automobile
                 </NavLink>
               </li>
               <li>
                 <NavLink
                   className="dropdown-item"
                   aria-current="page"
                   to="manufacturers/"
                 >
                   All Manufacturers
                 </NavLink>
               </li>
               <li>
                 <NavLink
                   className="dropdown-item"
                   aria-current="page"
                   to="manufacturers/new"
                 >
                   Add A Manufacturer
                 </NavLink>
               </li>
               <li>
                 <NavLink
                   className="dropdown-item"
                   aria-current="page"
                   to="vehiclelist/"
                 >
                   All Vehicles
                 </NavLink>
               </li>
               <li>
                 <NavLink
                   className="dropdown-item"
                   aria-current="page"
                   to="createvehicle/"
                 >
                   Add Vehicle
                 </NavLink>
               </li>
             </ul>
           </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <li>
                  <NavLink
                    className="dropdown-item"
                    activeClassName="active"
                    exact
                    to="/newsalesperson"
                  >
                    Create Sales Person
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    className="dropdown-item"
                    activeClassName="active"
                    exact
                    to="/Newcustomer"
                  >
                    New Customer
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    className="dropdown-item"
                    activeClassName="active"
                    exact
                    to="/CreateSaleRecord"
                  >
                    Create Sale Record
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    className="dropdown-item"
                    activeClassName="active"
                    exact
                    to="/AllSales"
                  >
                    All Sales
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    className="dropdown-item"
                    activeClassName="active"
                    exact
                    to="/SalesPersonSales"
                  >
                    Sales Person's Sales History
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Services
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <li>
                  <Link className="dropdown-item" to="appointments/new">
                    Create a Service Appointment
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="appointments/">
                    List of Appointments
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="history/">
                    Service History
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="technicians/new">
                    Create Technician
                  </Link>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
