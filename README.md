# CarCar

Team:

* Alex Hernandez - Auto Service
* Abdalla - Auto Sales

## Design

## Service microservice
The models in this microservice consist of 3 classes; the AutomobileVO model, the Techchnican model, and the Appointment Model.

1. The appointment model ultimately allows for the front end user to use its unique fields to create and appointment. These fields consist of a customers name, the reason for an appointment, the day and time for the appointment, and many more fields.

2. The technician model is simple, but useful. The fields allows a front end user to create a technician through a form with the unique fields of a technicians name and his

3. the AutomobileVO model allows for the vin field to retrieve very useful information about an automobile. With this vin number you can update appointments, view appointments, etc.


In conlusion the Service Microservice

- Allows an appointment service to be created with a form containing the customers information, reason, technician, etc.
- Gives you a list of appointments with the customers information from the form; also in the appointment list you can delete an appointment or mark it as finished , or see if the person is a VIP.
- Allows you to search Appointment history simply by searching with the Vin number
- And Lastly, a form is implemented to create a technician by adding the name of the technician and the employee number.



Explain your models and integration with the inventory
microservice, here.

## Sales microservice

Explain your models and integration with the inventory
microservice, here.

AutomobileVO: Represents a automobile and includes a VIN and an import URL. That it get form the automobile model. It uses a poller to grab the data from their that i can be used in the sales microservice.

SalesPersons: Represents a salesperson and includes a name and employee number.

Customer: Represents a customer and includes a name, address, and phone number.

Sale: Represents a sale transaction and includes foreign keys to an AutomobileVO, a SalesPersons, and a Customer object, as well as a price and a date of sale.
