from django.contrib import admin
from .models import AutomobileVO, SalesPersons, Sale, Customer


@admin.register(AutomobileVO)
class AutomobileAdmin(admin.ModelAdmin):
    pass

@admin.register(SalesPersons)
class SalesAdmin(admin.ModelAdmin):
    pass

@admin.register(Sale)
class ShoeAdmin(admin.ModelAdmin):
    pass

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass
